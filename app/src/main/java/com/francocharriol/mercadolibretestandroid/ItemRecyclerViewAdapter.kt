package com.francocharriol.mercadolibretestandroid

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.francocharriol.mercadolibretestandroid.models.ItemModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list.*
import kotlinx.android.synthetic.main.item_list_content.view.*
import kotlinx.android.synthetic.main.item_progress_bar.view.*
import java.text.NumberFormat
import java.util.*

/**
 * Created by Franko on 21/06/2018.
 */
class ItemRecyclerViewAdapter(private val mParentActivity: ItemListActivity,
                                    private var mValues: List<ItemModel?>,
                                    private val mTwoPane: Boolean) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val VIEW_ITEM = 1
    private val VIEW_PROG = 0
    private val mOnClickListener: View.OnClickListener
    private var recyclerView: RecyclerView? = null
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private var visibleThreshold = 5
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var loading: Boolean = false
    private var mOnLoadMoreListener: OnLoadMoreListener? = null

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ItemModel
            if (mTwoPane) {
                val fragment = ItemDetailFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(ItemDetailFragment.ARG_ITEM, item)
                    }
                }
                mParentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, ItemDetailActivity::class.java).apply {
                    putExtra(ItemDetailFragment.ARG_ITEM, item)
                }
                v.context.startActivity(intent)
            }
        }
    }
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView?) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView

        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val mLinearLayoutManager = recyclerView!!.layoutManager as LinearLayoutManager
                totalItemCount = mLinearLayoutManager.itemCount
                lastVisibleItem = mLinearLayoutManager.findLastVisibleItemPosition()

                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // End has been reached
                    // Do something
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener!!.onLoadMore()
                    }
                    loading = true
                }
            }
        })
    }

    override fun getItemViewType(position: Int): Int {
        return if (mValues[position] != null) VIEW_ITEM else VIEW_PROG
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if(viewType == VIEW_ITEM) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_list_content, parent, false)

            return ItemViewHolder(view)
        }else{
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_progress_bar, parent, false)

            return ProgressViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ItemViewHolder) {
            val item = mValues[position]
            if(item!!.thumbnail.isNotEmpty()){
                Picasso.get()
                        .load(item.thumbnail)
                        .fit().centerCrop()
                        .error(R.drawable.sin_foto)
                        .into(holder.mThumbnailImage)
            }else{
                holder.mThumbnailImage.setImageResource(R.drawable.sin_foto)
            }

            holder.mTitleText.text = item.title

            holder.mCurrencyText.text = mParentActivity.getString(mParentActivity.resources.getIdentifier(item.currencyId,"string",mParentActivity.packageName))

            val price = item.price.toString().split(".")
            holder.mPriceText.text = NumberFormat.getNumberInstance(Locale.getDefault()).format(price[0].toInt())
            holder.mPriceDecimalText.text =  if (price.size == 2 && item.price - item.price.toInt() > 0) price[1] else ""

            if(item.installments != null && item.installments.rate == 0.toDouble()){
                holder.mInstallmentsLayout.visibility = View.VISIBLE
                holder.mInstallmentIcon.setImageResource(R.drawable.ic_credit_card)
                holder.mInstallmentText.text = mParentActivity.getString(
                        R.string.installments_label,
                        item.installments.quantity
                )
            }else{
                holder.mInstallmentsLayout.visibility = View.GONE
                holder.mInstallmentIcon.setImageDrawable(null)
                holder.mInstallmentText.text = ""
            }

            if(item.freeShipping){
                holder.mShippingLayout.visibility = View.VISIBLE
                holder.mShippingIcon.setImageResource(R.drawable.ic_shipping_truck)
                holder.mShippingText.text =  mParentActivity.getString(R.string.free_shipping)
            } else {
                holder.mShippingLayout.visibility = View.GONE
                holder.mShippingIcon.setImageDrawable(null)
                holder.mShippingText.text =  ""
            }

            with(holder.itemView) {
                tag = item
                setOnClickListener(mOnClickListener)
            }
        } else {
            (holder as ProgressViewHolder).progressBar.isIndeterminate = true
        }
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    fun setList(mValues: List<ItemModel?>){
        this.mValues = mValues
    }

    fun setLoaded() {
        loading = false
    }

    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.mOnLoadMoreListener = onLoadMoreListener
    }

}

class ItemViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
    val mThumbnailImage: ImageView = mView.thumbnail_image
    val mTitleText: TextView = mView.title_text
    val mCurrencyText: TextView = mView.currency_text
    val mPriceText: TextView = mView.price_text
    val mPriceDecimalText: TextView = mView.price_decimal_text
    val mInstallmentsLayout: LinearLayout = mView.installments_layout
    val mInstallmentIcon: ImageView = mView.installments_icon
    val mInstallmentText: TextView = mView.installments_text
    val mShippingLayout: LinearLayout = mView.shipping_layout
    val mShippingIcon: ImageView = mView.shipping_icon
    val mShippingText: TextView = mView.shipping_text
}

class ProgressViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
    var progressBar: ProgressBar = mView.progress_bar_item
}
