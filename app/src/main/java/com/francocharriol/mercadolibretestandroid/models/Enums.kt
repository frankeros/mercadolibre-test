package com.francocharriol.mercadolibretestandroid.models

/**
 * Created by Franko on 18/06/2018.
 */

enum class  ShippingModes{
    me1, //send international
    me2, //send in the country
    not_specified,
    custom,
}

enum class Sorts{
    relevance,
    price_asc,
    price_desc
}