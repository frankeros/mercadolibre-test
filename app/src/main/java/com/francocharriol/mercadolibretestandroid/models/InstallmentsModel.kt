package com.francocharriol.mercadolibretestandroid.models

import java.io.Serializable
import java.util.*

/**
 * Created by Franko on 18/06/2018.
 */
data class InstallmentsModel (
        val quantity: Int,
        val amount: Double,
        val rate: Double,
        val currencyId: String
):Serializable