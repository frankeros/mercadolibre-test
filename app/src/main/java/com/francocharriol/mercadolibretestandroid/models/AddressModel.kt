package com.francocharriol.mercadolibretestandroid.models

import java.io.Serializable

/**
 * Created by Franko on 18/06/2018.
 */
data class AddressModel(
        val addressLine: String,
        val zipCode: String,
        val city: String,
        val state: String,
        val country: String
):Serializable