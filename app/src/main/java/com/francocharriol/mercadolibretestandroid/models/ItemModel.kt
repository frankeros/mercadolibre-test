package com.francocharriol.mercadolibretestandroid.models

import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Franko on 18/06/2018.
 */
data class ItemModel(
        val id: String,
        val title: String,
        val price: Double,
        val currencyId: String,
        val availableQuantity: Int,
        val soldQuantity: Int,
        val condition: String,
        val attributes: ArrayList<AttributeModel>,
        val link: String,
        val thumbnail: String,
        val installments : InstallmentsModel?,
        var pictures: ArrayList<String>,
        var videoId: String,
        var description: String,
        val shipping: String,
        val freeShipping: Boolean,
        val sellerAddress: AddressModel
        ):Serializable {
        companion object {

                fun getItemsArrayFromJSON(itemsJSON: JSONArray): ArrayList<ItemModel?> {
                        val itemsArray = ArrayList<ItemModel?>()

                        for (i in 0..(itemsJSON.length()-1)) {
                                val itemJSON = itemsJSON.getJSONObject(i)

                                val attributesArray = ArrayList<AttributeModel>()
                                val attributesJSON = itemJSON.getJSONArray("attributes")
                                for (j in 0..(attributesJSON.length() - 1)) {
                                        val attJSON = attributesJSON.getJSONObject(j)
                                        attributesArray.add(AttributeModel(
                                                attJSON.getString("name"),
                                                attJSON.getString("value_name")
                                        ))
                                }

                                var installments : InstallmentsModel? = null
                                if(!itemJSON.isNull("installments")){
                                        val installmentsJSON = itemJSON.getJSONObject("installments")
                                        installments = InstallmentsModel(
                                                installmentsJSON.getInt("quantity"),
                                                installmentsJSON.getDouble("amount"),
                                                installmentsJSON.getDouble("rate"),
                                                installmentsJSON.getString("currency_id")
                                        )
                                }

                                val sellerAddressJSON = itemJSON.getJSONObject("seller_address")
                                val sellerAddress = AddressModel(
                                        sellerAddressJSON.getString("address_line"),
                                        sellerAddressJSON.getString("zip_code"),
                                        sellerAddressJSON.getJSONObject("city").getString("name"),
                                        sellerAddressJSON.getJSONObject("state").getString("name"),
                                        sellerAddressJSON.getJSONObject("country").getString("name")
                                )

                                val item = ItemModel(
                                        itemJSON.getString("id"),
                                        itemJSON.getString("title"),
                                        itemJSON.getDouble("price"),
                                        itemJSON.getString("currency_id"),
                                        itemJSON.getInt("available_quantity"),
                                        itemJSON.getInt("sold_quantity"),
                                        itemJSON.getString("condition"),
                                        attributesArray,
                                        itemJSON.getString("permalink"),
                                        itemJSON.getString("thumbnail"),
                                        installments,
                                        ArrayList<String>(),  //this come in detail endpoint
                                        "", //this come in detail endpoint
                                        "", //this come in detail endpoint
                                        itemJSON.getJSONObject("shipping").getString("mode"),
                                        itemJSON.getJSONObject("shipping").getBoolean("free_shipping"),
                                        sellerAddress
                                )

                                itemsArray.add(item)
                        }
                        return itemsArray
                }
        }
}
