package com.francocharriol.mercadolibretestandroid.models

import java.io.Serializable

/**
 * Created by Franko on 18/06/2018.
 */
data class AttributeModel(
        val name: String,
        val value: String
):Serializable