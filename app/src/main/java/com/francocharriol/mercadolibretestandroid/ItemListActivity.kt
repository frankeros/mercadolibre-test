package com.francocharriol.mercadolibretestandroid

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.francocharriol.mercadolibretestandroid.models.ItemModel
import com.francocharriol.mercadolibretestandroid.models.Sorts
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.mercadolibre.android.sdk.ApiRequestListener
import com.mercadolibre.android.sdk.ApiResponse
import com.mercadolibre.android.sdk.Meli
import kotlinx.android.synthetic.main.activity_item_list.*
import kotlinx.android.synthetic.main.item_list.*
import org.json.JSONObject
import java.net.URLEncoder


class ItemListActivity : AppCompatActivity(), SearchView.OnQueryTextListener, ApiRequestListener {
    companion object {
        val SP_LAST_QUERY_SEARCH = "sp_last_query_search"
        val SP_LAST_SORT_SEARCH = "sp_last_sort_search"
        val SP_LAST_PAGING_SEARCH = "sp_last_paging_search"
        val SP_QUERYS_SEARCH = "sp_querys_search"
    }
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var mTwoPane: Boolean = false
    private var mItems : ArrayList<ItemModel?> = ArrayList()
    private var mAdapter: ItemRecyclerViewAdapter? = null
    private var querySearch: String = ""
    private var sortSearch: String = Sorts.relevance.toString()
    private var pagingTotal: Int = 0
    private var querysArray: ArrayList<String> = ArrayList()
    private var mQuerysArrayAdapter : ArrayAdapter<String>? = null
    private val gson = GsonBuilder().setPrettyPrinting().create()
    private var prefs:  SharedPreferences? = null

    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        Meli.initializeSDK(applicationContext)

        setSupportActionBar(toolbar)

        querySearch =  if(!intent.getStringExtra(MainActivity.QUERY_SEARCH).isNullOrEmpty()) intent.getStringExtra(MainActivity.QUERY_SEARCH) else ""

        prefs = applicationContext.getSharedPreferences(MainActivity.PREFS_FILENAME,Context.MODE_PRIVATE)

        if(!querySearch.isEmpty()) {
            sortSearch = Sorts.relevance.toString()
            pagingTotal = 0
        }else{
            querySearch = prefs!!.getString(ItemListActivity.SP_LAST_QUERY_SEARCH,"ipod")
            sortSearch = prefs!!.getString(ItemListActivity.SP_LAST_SORT_SEARCH,Sorts.relevance.toString())
            pagingTotal = prefs!!.getInt(ItemListActivity.SP_LAST_PAGING_SEARCH, 0)
        }

        supportActionBar!!.title = querySearch

        item_list.setHasFixedSize(true)
        item_list.addItemDecoration( DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        item_list.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        mAdapter = ItemRecyclerViewAdapter(this, mItems, mTwoPane)
        item_list.adapter = mAdapter

        mAdapter!!.setOnLoadMoreListener(object: OnLoadMoreListener{
            override fun onLoadMore() {
                super.onLoadMore()
                if(mItems.size < pagingTotal) {
                    getItems(mItems.size)
                }else{
                    mAdapter!!.setLoaded()
                }
            }
        })

        if (item_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true
        }

        val querysJSON = prefs!!.getString(ItemListActivity.SP_QUERYS_SEARCH,"")

        if(!querysJSON.isNullOrEmpty()) {
            querysArray.addAll(gson.fromJson(querysJSON, object : TypeToken<ArrayList<String>>() {}.type))
        }




    }

    override fun onStart() {
        super.onStart()

        Handler().postDelayed({
            getItems(0)
        }, 300)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.search_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchMenuItem = menu!!.findItem(R.id.action_search)
        val searchView = searchMenuItem.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.isSubmitButtonEnabled = true
        searchView.setQuery(querySearch,false)
        searchView.queryHint = getString(R.string.search_hint)
        searchView.setOnQueryTextListener(this)


        val searchAutoComplete  = searchView.findViewById<SearchView.SearchAutoComplete>(android.support.v7.appcompat.R.id.search_src_text)
        searchAutoComplete.setBackgroundColor(Color.WHITE)
        searchAutoComplete.setTextColor(Color.BLACK)
        searchAutoComplete.setDropDownBackgroundResource(R.drawable.background_white)
        searchAutoComplete.dropDownAnchor = R.id.action_search

        mQuerysArrayAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, querysArray)
        Log.d("querysArray",querysArray.size.toString())
        searchAutoComplete.setAdapter(mQuerysArrayAdapter!!)

        // Listen to search view item on click event.
        searchAutoComplete.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, itemIndex, id ->
            val queryString = adapterView.getItemAtPosition(itemIndex) as String
            searchAutoComplete.setText("" + queryString)
        }

        return true
    }

    override fun onPause() {
        super.onPause()
        with(prefs!!.edit()) {
            putString(ItemListActivity.SP_LAST_QUERY_SEARCH, querySearch)
            putString(ItemListActivity.SP_LAST_SORT_SEARCH, sortSearch)
            putInt(ItemListActivity.SP_LAST_PAGING_SEARCH, pagingTotal)
            putString(ItemListActivity.SP_QUERYS_SEARCH,  gson.toJson(querysArray, object : TypeToken<ArrayList<String>>() {}.type).toString())
            commit()
        }
    }

    private fun getItems(offset: Int) {
        val LIMIT_ITEMS = 30
        var path: String = "/sites/MLA/search?q=${URLEncoder.encode(querySearch, "UTF-8")}&limit=$LIMIT_ITEMS&offset=$offset"

        //show de progress Item
        if (offset > 0){
            mItems.add(null)
            item_list.post({ mAdapter!!.notifyItemInserted(mItems.size - 1) })
        } else{
            mItems.clear()
            mAdapter!!.notifyDataSetChanged()
            progress_bar.visibility = View.VISIBLE
        }

        Meli.asyncGet(path, this)
    }

    override fun onRequestProcessed(requestCode: Int, payload: ApiResponse?) {
        //hide de progress Item
        if (mItems.size > 0 && mItems.last() == null){
            mItems.removeAt(mItems.size - 1)
            item_list.post({
                mAdapter!!.notifyItemRemoved(mItems.size - 1)
            })
            mAdapter!!.setLoaded()
        }else{
            progress_bar.visibility = View.GONE
        }

        if (payload != null) {
            if(payload.responseCode  == ApiResponse.ApiResponseCode.RESPONSE_CODE_SUCCESS){
                val content = JSONObject(payload.content)
                val pagingJSON = content.getJSONObject("paging")
                pagingTotal = pagingJSON.getInt("total")
                sortSearch = content.getJSONObject("sort").getString("id")

                val itemsJSON = content.getJSONArray("results")
                mItems.addAll(ItemModel.getItemsArrayFromJSON(itemsJSON))
                //mAdapter!!.setList(mItems)
                mAdapter!!.notifyDataSetChanged()
            } else{
                Snackbar.make(item_list, getString(R.string.error_response), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
            }
        }else
        {
            Snackbar.make(item_list, getString(R.string.error_response), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    override fun onRequestStarted(requestCode: Int) {
    }


    override fun onQueryTextSubmit(p0: String?): Boolean {
        querySearch = p0!!
        //sortSearch = Sorts.relevance.toString()
        pagingTotal = 0

        if(querysArray.contains(p0)){
            querysArray.removeAt((querysArray.indexOf(p0)))
            querysArray.add(0,p0)
        }else{
            querysArray.add(0,p0)
        }

        mQuerysArrayAdapter!!.notifyDataSetChanged()

        getItems(0)
        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        return false
    }



}
