package com.francocharriol.mercadolibretestandroid

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Franko on 20/06/2018.
 */
class MainActivity : AppCompatActivity() {
    companion object {
        val PREFS_FILENAME = "com.francocharriol.mercadolibretestandroid.prefs"
        val QUERY_SEARCH : String = "QUERY_SEARCH"
    }

    private var querysArray: ArrayList<String> = ArrayList()
    private var mQuerysArrayAdapter : ArrayAdapter<String>? = null
    private val gson = GsonBuilder().setPrettyPrinting().create()
    private var prefs: SharedPreferences? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prefs = applicationContext.getSharedPreferences(MainActivity.PREFS_FILENAME, Context.MODE_PRIVATE)

        val querysJSON = prefs!!.getString(ItemListActivity.SP_QUERYS_SEARCH,"")

        if(!querysJSON.isNullOrEmpty()) {
            querysArray.addAll(gson.fromJson(querysJSON, object : TypeToken<ArrayList<String>>() {}.type))
        }

        mQuerysArrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1 , querysArray)
        search_edit_text.setAdapter(mQuerysArrayAdapter!!)
        search_edit_text.threshold = 1

        // Listen to search view item on click event.
        search_edit_text.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, itemIndex, id ->
            val queryString = adapterView.getItemAtPosition(itemIndex) as String
            search_edit_text.setText("" + queryString)
        }
        
        search_button.setOnClickListener({
            if(search_edit_text.text.isNotEmpty()){
                val query = search_edit_text.text.toString()
                if(querysArray.contains(query)){
                    querysArray.removeAt((querysArray.indexOf(query)))
                    querysArray.add(0,query)
                }else{
                    querysArray.add(0,query)
                }

                val intent = Intent(this,ItemListActivity::class.java)
                intent.putExtra(QUERY_SEARCH,search_edit_text.text.toString())
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            }
        })
    }

    override fun onPause() {
        super.onPause()
        with(prefs!!.edit()) {
            putString( ItemListActivity.SP_QUERYS_SEARCH, gson.toJson(querysArray, object : TypeToken<ArrayList<String>>() {}.type).toString())
            commit()
        }

    }
}