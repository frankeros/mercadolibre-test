package com.francocharriol.mercadolibretestandroid

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.francocharriol.mercadolibretestandroid.models.AttributeModel
import com.francocharriol.mercadolibretestandroid.models.ItemModel
import com.francocharriol.mercadolibretestandroid.models.ShippingModes
import com.mercadolibre.android.sdk.ApiRequestListener
import com.mercadolibre.android.sdk.ApiResponse
import com.mercadolibre.android.sdk.Meli
import kotlinx.android.synthetic.main.item_detail.*
import kotlinx.android.synthetic.main.item_detail.view.*
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.view.Gravity


class ItemDetailFragment : Fragment() {
    companion object {
        const val ARG_ITEM = "item"
    }

    private var mItem: ItemModel? = null
    private var mPictures: ArrayList<String?> = ArrayList<String?>()
    private var currentPage: Int = 0
    private val handler = Handler()
    private val swipeTimer = Timer()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_ITEM)) {
                mItem = it.getSerializable(ARG_ITEM) as ItemModel
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.item_detail, container, false)

        activity!!.title = mItem!!.title

        rootView.title_text.text = mItem!!.title

        rootView.currency_text.text = activity.getString(activity.resources.getIdentifier(mItem!!.currencyId,"string",activity.packageName))

        val price = mItem!!.price.toString().split(".")
        rootView.price_text.text = NumberFormat.getNumberInstance(Locale.getDefault()).format(price[0].toInt())
        rootView.price_decimal_text.text =  if (price.size == 2 && mItem!!.price - mItem!!.price.toInt() > 0) price[1] else ""

        if(mItem!!.installments != null && mItem!!.installments!!.rate == 0.toDouble()){
            rootView.installments_layout.visibility = View.VISIBLE
            rootView.installments_icon.setImageResource(R.drawable.ic_credit_card)
            rootView.installments_text.text = activity.getString(
                    R.string.installments_label,
                    mItem!!.installments!!.quantity
            )
        }else{
            rootView.installments_layout.visibility = View.GONE
            rootView.installments_icon.setImageDrawable(null)
            rootView.installments_text.text = ""
        }

        if(mItem!!.freeShipping){
            rootView.shipping_layout.visibility = View.VISIBLE
            rootView.shipping_icon.setImageResource(R.drawable.ic_shipping_truck)
            rootView.shipping_icon.setColorFilter(R.color.colorGreen, android.graphics.PorterDuff.Mode.MULTIPLY)
            rootView.shipping_text.text =  activity.getString(R.string.free_shipping)
            rootView.shipping_text.setTextColor(ContextCompat.getColor(activity,R.color.colorGreen))
        } else {
            when(mItem!!.shipping){
                ShippingModes.me2.toString() -> {
                    rootView.shipping_layout.visibility = View.VISIBLE
                    rootView.shipping_icon.setImageResource(R.drawable.ic_shipping_fly)
                    rootView.shipping_icon.setColorFilter(R.color.colorAccent, android.graphics.PorterDuff.Mode.MULTIPLY)
                    rootView.shipping_text.text = activity.getString(R.string.international_shipping)
                    rootView.shipping_text.setTextColor(Color.DKGRAY)

                }
                ShippingModes.me2.toString() -> {
                    rootView.shipping_layout.visibility = View.VISIBLE
                    rootView.shipping_icon.setImageResource(R.drawable.ic_shipping_truck)
                    rootView.shipping_icon.setColorFilter(R.color.colorAccent, android.graphics.PorterDuff.Mode.MULTIPLY)
                    rootView.shipping_text.text = activity.getString(R.string.national_shipping)
                    rootView.shipping_text.setTextColor(Color.DKGRAY)
                }
                else ->{
                    rootView.shipping_layout.visibility = View.GONE
                    rootView.shipping_icon.setImageDrawable(null)
                    rootView.shipping_text.text =  ""
                }
            }
        }

        val sellerLocation = mItem!!.sellerAddress
        var sellerLocationText = ""
        sellerLocationText = if(sellerLocation.addressLine.isNotEmpty()) "$sellerLocationText${sellerLocation.addressLine}" else ""
        sellerLocationText = if(sellerLocation.zipCode.isNotEmpty()) "${if(sellerLocationText.isEmpty()) "" else "$sellerLocationText, "}${sellerLocation.zipCode}" else sellerLocationText
        sellerLocationText = if(sellerLocation.city.isNotEmpty()) "${if(sellerLocationText.isEmpty()) "" else "$sellerLocationText, "}${sellerLocation.city}" else sellerLocationText
        sellerLocationText = if(sellerLocation.state.isNotEmpty()) "${if(sellerLocationText.isEmpty()) "" else "$sellerLocationText, "}${sellerLocation.state}" else sellerLocationText
        sellerLocationText = if(sellerLocation.country.isNotEmpty()) "${if(sellerLocationText.isEmpty()) "" else "$sellerLocationText, "}${sellerLocation.country}" else sellerLocationText

        rootView.location_text.text = sellerLocationText

        rootView.attributes_grid.adapter = AttributesArrayAdapter(activity,mItem!!.attributes)
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            rootView.attributes_grid.isFocusable = false
        //}

        val params : CoordinatorLayout.LayoutParams = CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.WRAP_CONTENT,
                CoordinatorLayout.LayoutParams.WRAP_CONTENT
        )
        params.gravity = Gravity.BOTTOM or Gravity.END
        params.marginEnd = (resources.getDimension(R.dimen.fab_margin) / resources.getDisplayMetrics().density).toInt()
        params.bottomMargin = (resources.getDimension(R.dimen.fab_margin) / resources.getDisplayMetrics().density).toInt()

        val openInBrowserFab = FloatingActionButton(activity)
        openInBrowserFab.layoutParams = params
        openInBrowserFab.setImageResource(R.drawable.ic_open_browser)

        openInBrowserFab.setOnClickListener { view ->
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(mItem!!.link))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.`package` = "com.android.chrome"
            try {
                context.startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                // Chrome browser presumably not installed and open Kindle Browser
                intent.`package` = "com.amazon.cloud9"
                context.startActivity(intent)
            }

        }

        val activityView = activity.findViewById<CoordinatorLayout>(R.id.activity_root_layout)
        activityView.addView(openInBrowserFab)

        this.getItemDetail()

        return rootView
    }

    override fun onPause() {
        super.onPause()
        swipeTimer.cancel()
    }

    private fun initSlide(){
        view!!.indicator.setViewPager(pictures_pager)
        val density = resources.displayMetrics.density

        //Set circle indicator radius
        view!!.indicator.radius = 5 * density
        currentPage = 0
        // Auto start of viewpager
        val update = Runnable {
            if (currentPage === mPictures.size) {
                currentPage = 0
            }
            view!!.pictures_pager.setCurrentItem(currentPage++, true)
        }

        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, 3000, 3000)

        // Pager listener over indicator
        view!!.indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position
            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })
    }

    private fun getItemDetail(){
        val path: String = "/items/${mItem!!.id}"

        Meli.asyncGet(path, object: ApiRequestListener {
            override fun onRequestProcessed(requestCode: Int, payload: ApiResponse?) {
                if (payload != null) {
                    if(payload.responseCode == ApiResponse.ApiResponseCode.RESPONSE_CODE_SUCCESS){
                        val content = JSONObject(payload.content)
                        Log.d("JSON content", content.toString())
                        val picturesJSON = content.getJSONArray("pictures")
                        for(i in 0..(picturesJSON.length()-1)){
                            val pictureJSON = picturesJSON.getJSONObject(i)
                            mItem!!.pictures.add(pictureJSON.getString("url"))
                        }

                        mPictures.addAll(mItem!!.pictures)
                        if(mPictures.size == 0) {
                            mPictures.add(null)
                        }

                        view!!.pictures_pager.adapter = SlidingImageAdapter(activity,mPictures)
                        initSlide()

                        mItem!!.videoId = content.getString("video_id")
                        val descriptionId = content.getJSONArray("descriptions").getJSONObject(0).getString("id")
                        getItemDescription(descriptionId)
                    }else{
                        Snackbar.make(view!!, context!!.getString(R.string.error_response_detail), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                    }
                }else
                {
                    Snackbar.make(view!!, context!!.getString(R.string.error_response_detail), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                }
            }

            override fun onRequestStarted(requestCode: Int) {

            }
        })
    }

    private fun getItemDescription(descriptionId: String){
        val path: String = "/items/$id/descriptions/$descriptionId"

        Meli.asyncGet(path, object: ApiRequestListener {
            override fun onRequestProcessed(requestCode: Int, payload: ApiResponse?) {
                if (payload != null) {
                    if(payload.responseCode  == ApiResponse.ApiResponseCode.RESPONSE_CODE_SUCCESS){
                        val content = JSONObject(payload.content)
                        mItem!!.description = content.getString("plain_text")

                        view!!.item_detail.text = mItem!!.description
                    }else{
                        Snackbar.make(view!!, context!!.getString(R.string.error_response_description), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                    }
                }else
                {
                    Snackbar.make(view!!, context!!.getString(R.string.error_response_description), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                }
            }

            override fun onRequestStarted(requestCode: Int) {

            }
        })
    }

    inner class  AttributesArrayAdapter(private val mContext: Context, private val mAttributes: List<AttributeModel>) : BaseAdapter() {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

            val inflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            var gridView: View

            if (convertView == null) {

                gridView = View(context)

                // get layout from mobile.xml
                gridView = inflater.inflate(R.layout.attribute_item, null)
                val attribute = mAttributes[position]

                val nameText = gridView
                        .findViewById<TextView>(R.id.attribute_name_text)
                nameText.text = "${attribute.name}:"

                val valueText = gridView
                        .findViewById<TextView>(R.id.attribute_value_text)
                valueText.text = attribute.value

            } else {
                gridView = convertView
            }

            return gridView
        }

        override fun getCount(): Int {
            return mAttributes.size
        }

        override fun getItem(position: Int): Any? {
            return null
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

    }
}
