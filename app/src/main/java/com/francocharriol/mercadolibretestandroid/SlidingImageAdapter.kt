package com.francocharriol.mercadolibretestandroid

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.os.Parcelable
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.slidingimages_layout.view.*


/**
 * Created by Franko on 22/06/2018.
 */
class SlidingImageAdapter(private val context: Context, private val images: ArrayList<String?>) : PagerAdapter() {
    private var inflater: LayoutInflater? = null

    init{
        inflater = LayoutInflater.from(context)

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater!!.inflate(R.layout.slidingimages_layout, view, false)!!

        if(images[position] != null) {
            Picasso.get()
                    .load(images[position])
                    //.fit().centerCrop()
                    .error(R.drawable.sin_foto)
                    .into(imageLayout.picture_image)
        }else{
            imageLayout.picture_image.setImageResource(R.drawable.sin_foto)
        }

        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }
}